import express from "express";
import bodyParser from "body-parser";
import { promises as fs } from "fs";
import * as path from 'path';
(async () => {
  const app = express();
  const port = process.env.PORT || 8080; // default port to listen
  app.listen(port, () => {
    console.log(`server running localhost:${port}`);
    console.log(`press CTRL+C to stop server`);
  });

  app.use(bodyParser.json({ limit: "20mb" }));

  app.get("/users", async (req, res) => {
    const { first_name, last_name } = req.query;
    console.log({first_name, last_name});
    const rawData = await fs.readFile(path.join(__dirname, `../data/user.json`));
    const users = JSON.parse(rawData.toString());
    const requestFirstName = first_name ? first_name : "";
    const requestLastName = last_name ?? "";
    const user = users.filter((u: any) =>
      u.first_name.toLowerCase() === (requestFirstName as string).toLowerCase()
      || u.last_name.toLowerCase() === (requestLastName as string).toLowerCase())
    return res.send(user);
  });

  app.get("/users", async (req, res) => {
    const rawData = await fs.readFile(path.join(__dirname, `../data/user.json`));
    const users = JSON.parse(rawData.toString());
    return res.send(users);
  });


  app.get("/users/:first_name", async (req, res) => {
    const rawData = await fs.readFile(path.join(__dirname, `../data/user.json`));
    const users = JSON.parse(rawData.toString());
    const params = req.params;
    const requestFirstName = params.first_name;
    const user = users.filter((u: any) => u.first_name.toLowerCase() === requestFirstName.toLocaleLowerCase())
    return res.send(user);
  });


  app.get("/", async (req, res) => {
    return res.send({ message: "welcome" });
  });
})();


